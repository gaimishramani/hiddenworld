﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dotmob.HiddenWord
{
	public class Position
	{
		public int row;
		public int col;

		public Position(int row, int col)
		{
			this.row = row;
			this.col = col;
		}
	}
}
