﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CategoryInfo
{
	#region Enums

	public enum LockType
	{
		None,
		Coins,
		IAP
	}

	#endregion

	public string			displayName;
	public string			saveId;
	public Sprite			icon;
	public Color			categoryColor;	
	public TextAsset		wordFile;
	public List<TextAsset>	levelFiles;
	public LockType			lockType;
	public int				unlockAmount;
	public string			iapProductId;
}
