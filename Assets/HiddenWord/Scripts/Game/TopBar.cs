﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dotmob.HiddenWord
{
	public class TopBar : MonoBehaviour
	{
		#region Inspector Variables

		[SerializeField] private CanvasGroup	backButton				= null;
		[SerializeField] private CanvasGroup settingButton = null;
		[SerializeField] private CanvasGroup	mainScreenContainer		= null;
		[SerializeField] private CanvasGroup	categoryContainer		= null;
		[SerializeField] private Text			categoryNameText		= null;
		[SerializeField] private Text			levelNumberText			= null;
		[SerializeField] private Text			coinAmountText			= null;
		[SerializeField] private Text			keyAmountText			= null;

		#endregion

		#region Member Variables

		#endregion

		#region Properties

		#endregion

		#region Unity Methods

		private void Update()
		{
			coinAmountText.text		= "" + GameManager.Instance.Coins.ToString();
			keyAmountText.text		= "" + GameManager.Instance.Keys.ToString();
			levelNumberText.text	= "Level " + (GameManager.Instance.ActiveLevelIndex + 1);

			ScreenManager.Instance.OnSwitchingScreens = OnSwitchingScreens;
		}

		#endregion

		#region Public Methods

		private void Start()
		{
			backButton.alpha = 0f;
		}

		#endregion

		#region Protected Methods

		#endregion

		#region Private Methods

		private void OnSwitchingScreens(string fromScreenId, string toScreenId)
		{

            

			if (fromScreenId == "main")
			{
				backButton.alpha = 1f;
				backButton.gameObject.SetActive(true);
				settingButton.alpha = 0f;
				settingButton.gameObject.SetActive(false);
				//FadeIn(backButton);
				FadeOut(mainScreenContainer);
				FadeIn(categoryContainer);

				if (GameManager.Instance.ActiveCategoryInfo != null)
				{
					categoryNameText.text = GameManager.Instance.ActiveCategoryInfo.displayName ;
				}


				if (toScreenId == "home")
				{
					Debug.Log("HERE");
					backButton.alpha = 0f;
					backButton.gameObject.SetActive(false);
					settingButton.alpha = 1f;
					settingButton.gameObject.SetActive(true);
					FadeOut(mainScreenContainer);
					FadeOut(categoryContainer);
                }
               

			}
			else if (toScreenId == "main")
			{
				backButton.alpha = 1f;
				backButton.gameObject.SetActive(true);
				settingButton.alpha = 0f;
				settingButton.gameObject.SetActive(false);
				///FadeOut(backButton);
				FadeIn(mainScreenContainer);
				FadeOut(categoryContainer);
			}

			if (toScreenId == "game")
			{
				backButton.alpha = 0f;
				backButton.gameObject.SetActive(false);
				settingButton.alpha = 1f;
				settingButton.gameObject.SetActive(true);
				levelNumberText.gameObject.SetActive(GameManager.Instance.ActiveGameMode == GameManager.GameMode.Progress);
			}
			else if (fromScreenId == "game")
			{
				backButton.alpha = 1f;
				backButton.gameObject.SetActive(true);
				settingButton.alpha = 0f;
				settingButton.gameObject.SetActive(false);
				levelNumberText.gameObject.SetActive(false);
			}
		}

		private void FadeIn(CanvasGroup canvasGroup)
		{
			UIAnimation anim = UIAnimation.Alpha(canvasGroup, 0f, 1f, 0.5f);
			anim.style = UIAnimation.Style.EaseOut;
			anim.Play();
		}

		private void FadeOut(CanvasGroup canvasGroup)
		{
			UIAnimation anim = UIAnimation.Alpha(canvasGroup, 1f, 0f, 0.5f);
			anim.style = UIAnimation.Style.EaseOut;
			anim.Play();
		}

		#endregion
	}
}
