﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dotmob.HiddenWord
{
	public class SelectedWord : MonoBehaviour
	{
		#region Inspector Variables

		[SerializeField] private Text		selectedWordText;
		[SerializeField] private GameObject	selectedWordContainer;
		[SerializeField] private Image		selectedWordBkgImage;

		#endregion

		#region Public Methods

		public void SetSelectedWord(string word, Color color)
		{
			selectedWordText.text = word;
			selectedWordContainer.SetActive(true);

			selectedWordBkgImage.color = color;
		}

		public void Clear()
		{
			selectedWordText.text = "";
			selectedWordContainer.SetActive(false);
		}

		#endregion
	}
}
