﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace dotmob.HiddenWord
{
	public class WordListItem : MonoBehaviour
	{
		#region Inspector Variables

		[SerializeField] private Text		wordText;
		[SerializeField] private GameObject	foundIndicator;

		#endregion

		#region Public Methods

		public void Setup(string word)
		{
			wordText.text = word;
			foundIndicator.SetActive(false);
		}

		public void SetWordFound()
		{
			foundIndicator.SetActive(true);
		}

		#endregion
	}
}
