﻿using UnityEngine;
using System.Collections;

namespace dotmob
{
    public class ButtonSoundToggle : TButton
    {

        protected override void Start()
        {
            base.Start();
            //IsOn = Sound.instance.IsEnabled();
        }

        public override void OnButtonClick()
        {
            base.OnButtonClick();
            SoundManager.Instance.SetSoundTypeOnOff(SoundManager.SoundType.SoundEffect, isOn);
        }
    }
}